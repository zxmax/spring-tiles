package net.zxmax.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController {
	public static final Logger LOGGER = LoggerFactory.getLogger(net.zxmax.controller.HelloWorldController.class);

	@RequestMapping("/hello")
	public ModelAndView helloWorld() {

		String message = "valore_assegnato_in_HelloWorldController";
		LOGGER.debug(message);
		return new ModelAndView("hello", "nome_variabile_assegnato_in_HelloWorldController", message);
	}

}
