package net.zxmax.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import net.zxmax.form.FormData;

@Controller
@SessionAttributes
public class ControllerDue {

	public static final Logger LOGGER = LoggerFactory.getLogger(net.zxmax.controller.ControllerDue.class);

	@RequestMapping(value = "/addItem", method = RequestMethod.POST)
	public String addItem(@ModelAttribute("mio_form") FormData formData, BindingResult result) {

		LOGGER.trace("mia stringa:" + formData.getMiaStringa());

		return "redirect:lista";
	}

	@RequestMapping(value = { "/lista" }, method = RequestMethod.GET)
	public ModelAndView lista() {

		return new ModelAndView("lista", "command", new FormData());
	}
}
