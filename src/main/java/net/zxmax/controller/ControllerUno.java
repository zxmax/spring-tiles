package net.zxmax.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerUno {

	@RequestMapping(value = { "/" }, method = RequestMethod.GET)
	public ModelAndView un_nome_che_mi_piace() {
		return new ModelAndView("mia_index");
	}
}
